# generate-random-ints

Generate random integers, given a PRNG seed and number of integers to generate; used to demonstrate submodule pipelining: https://gitlab.com/gsmo/examples/submodule-demo